package com.dsc.ssm.pojo;

import lombok.Data;

/**
 * @author Du Shun Chang
 * @version 1.0
 * @date 2021/6/30 19:57
 * @Todo:用户实体类
 */
@Data
public class User {
   /**
    *
    * @author Du Shun Chang
    * @date 2021/7/3 23:02
    * @param id 用户id
    * @return null
    */
    private Integer id;

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/3 23:05
     * @param name 用户姓名
     * @return null
     */
    private String name;

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/3 23:07
     * @param sex  用户性别
     * @return null
     */
    private String sex;

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/3 23:07
     * @param email 用户邮箱
     * @return null
     */
    private String email;

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/3 23:06
     * @param 用户密码
     * @return null
     */
    private String pwd;

  /**
   *
   * @author Du Shun Chang
   * @date 2021/7/3 23:06
   * @param phone 用户手机号码
   * @return null
   */
    private String phone;
}
