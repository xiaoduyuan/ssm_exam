package com.dsc.ssm.service;


import com.dsc.ssm.pojo.User;

import java.util.List;

/**
 * @author Du Shun Chang
 * @version 1.0
 * @date 2021/7/3 23:09
 * @Todo:
 */
public interface UserService {
    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:19
     * @param user
     * @param login 用户登录接口
     * @return com.dsc.ssm.pojo.User
     */
    User login(User user);

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:21
     * @param user
     * @param reg 用户注册接口
     */
    void reg(User user);

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:23
     * @return java.util.List<com.dsc.ssm.pojo.User>
     */
    List<User> findUser();

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:24
     * @param id
     * @return com.dsc.ssm.pojo.User
     */
    User findById(Integer id);

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:24
     * @param id
     * @return int
     */
    int delete(Integer id);

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:24
     * @param user
     * @return int
     */
    int insert(User user);

    /**
     *
     * @author Du Shun Chang
     * @date 2021/7/7 10:25
     * @param user
     * @return int
     */
    int update(User user);



}
